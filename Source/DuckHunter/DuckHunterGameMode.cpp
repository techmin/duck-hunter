// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DuckHunterGameMode.h"
#include "DuckHunterHUD.h"
#include "DuckHunterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADuckHunterGameMode::ADuckHunterGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADuckHunterHUD::StaticClass();
}
