// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DUCKHUNTER_DuckHunterCharacter_generated_h
#error "DuckHunterCharacter.generated.h already included, missing '#pragma once' in DuckHunterCharacter.h"
#endif
#define DUCKHUNTER_DuckHunterCharacter_generated_h

#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_SPARSE_DATA
#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_RPC_WRAPPERS
#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADuckHunterCharacter(); \
	friend struct Z_Construct_UClass_ADuckHunterCharacter_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterCharacter)


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesADuckHunterCharacter(); \
	friend struct Z_Construct_UClass_ADuckHunterCharacter_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterCharacter)


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADuckHunterCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADuckHunterCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterCharacter(ADuckHunterCharacter&&); \
	NO_API ADuckHunterCharacter(const ADuckHunterCharacter&); \
public:


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterCharacter(ADuckHunterCharacter&&); \
	NO_API ADuckHunterCharacter(const ADuckHunterCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADuckHunterCharacter)


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ADuckHunterCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ADuckHunterCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ADuckHunterCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ADuckHunterCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ADuckHunterCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ADuckHunterCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ADuckHunterCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ADuckHunterCharacter, L_MotionController); }


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_11_PROLOG
#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_SPARSE_DATA \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_RPC_WRAPPERS \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_INCLASS \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_SPARSE_DATA \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_INCLASS_NO_PURE_DECLS \
	DuckHunter_Source_DuckHunter_DuckHunterCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DUCKHUNTER_API UClass* StaticClass<class ADuckHunterCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DuckHunter_Source_DuckHunter_DuckHunterCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
